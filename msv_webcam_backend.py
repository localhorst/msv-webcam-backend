#!/usr/bin/env python3
# -*- coding: utf-8 -*-
""" Author:                     Hendrik Schutter, mail@hendrikschutter.com
"""

import glob
import os
import time
import shutil
import sys

signal_pipe_path = "/tmp/msv-webcam-signal-pipe"
signal_pipe_msg = "msv-webcam-new-images"

def copy_image(src, destination_dir_path, destination_owner):
    # print("src: " + str(src))
    # print("dst: " + str(destination_dir_path))
    os.makedirs(os.path.dirname(destination_dir_path), exist_ok=True)
    shutil.copyfile(src, destination_dir_path)
    os.chmod(destination_dir_path, 0o0644)
    shutil.chown(destination_dir_path, destination_owner)

def signal_new_images():
    if not os.path.exists(signal_pipe_path):
        os.mkfifo(signal_pipe_path)
    with open(signal_pipe_path, "w") as f:
        f.write(signal_pipe_msg)
        f.close()

def main():
    print("starting ...")

    update_interval = -1
    destination_dir_path = -1
    destination_owner = -1
    search_dirs = list()

    for argument in sys.argv:
        if argument.startswith('interval'):
            update_interval = int(argument.split("=")[1])
        if argument.startswith('destination_dir'):
            destination_dir_path = argument.split("=")[1]
        if argument.startswith('destination_owner'):
            destination_owner = argument.split("=")[1]
        if argument.startswith('search_dir'):
            tmp_search_dir = {
            "search_dir_path" : argument.split("=")[1],
            "newest_file_path" : "",
            "newest_file_size" : 0
            }
            search_dirs.append(tmp_search_dir)

    if ((update_interval == -1) or (destination_dir_path == -1) or (destination_owner == -1) or (len(search_dirs) == 0)):
        print("Unable to parse config!")
        print("Example usage:")
        print("     python msv_webcam_backend.py interval=5 search_dir=test_images/msvcamS search_dir=test_images/msvcamN destination_dir=/tmp/msv_webcam_current destination_owner=hendrik")
        sys.exit(-1)

    while True:
        new_images_found = False
        for search_dir in search_dirs:
            try:
                # print("\nSearch dir: " + str(search_dir["search_dir_path"]))
                list_of_files = glob.glob(search_dir["search_dir_path"]+"/**/*.jpg", recursive=True)
                # print(list_of_files)
                newest_file_path_tmp = max(list_of_files, key=os.path.getctime)
                newest_file_size_tmp =  os.stat(newest_file_path_tmp).st_size
                list_of_files.clear()

                if (newest_file_path_tmp == search_dir["newest_file_path"]) and (newest_file_size_tmp == search_dir["newest_file_size"]):
                    # already found the newest file
                    print("no newer file found")
                else:
                    time.sleep(1) # wait 1sec to see if the upload is still in progress

                    list_of_files = glob.glob(search_dir["search_dir_path"]+"/**/*.jpg", recursive=True)
                    newest_file_path_tmp_delayed = max(list_of_files, key=os.path.getctime)
                    newest_file_size_tmp_delayed =  os.stat(newest_file_path_tmp_delayed).st_size
                    list_of_files.clear()

                    if (newest_file_path_tmp == newest_file_path_tmp_delayed) and (newest_file_size_tmp == newest_file_size_tmp_delayed):
                        search_dir["newest_file_path"] = newest_file_path_tmp_delayed
                        search_dir["newest_file_size"] = newest_file_size_tmp_delayed
                        print("Found new file:")
                        print("Name: " + str(search_dir["newest_file_path"]))
                        print("Size: " + str(search_dir["newest_file_size"]))
                        dst_file_tmp = os.path.join(destination_dir_path, (os.path.basename(os.path.normpath(search_dir["search_dir_path"]))  + os.path.splitext(search_dir["newest_file_path"])[1]))
                        copy_image(search_dir["newest_file_path"], dst_file_tmp, destination_owner)
                        new_images_found = True
                        #break
                    else:
                        print("Upload not finished yet!")
            except Exception as e:
                print(e)
        #break
        if(new_images_found == True):
            signal_new_images()
        time.sleep(update_interval)
        
if __name__ == "__main__":
        main()
