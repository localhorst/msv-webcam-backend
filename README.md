# msv-webcam-backend

- `mkdir /opt/msv-webcam-backend/`
- `cd /opt/msv-webcam-backend/`
- import `msv_webcam_backend.py`
- `chmod +x /opt/msv-webcam-backend/msv_webcam_backend.py` 
- `nano /etc/systemd/system/msv-webcam-backend.service`
- `systemctl daemon-reload && systemctl enable --now msv-webcam-backend.service`
